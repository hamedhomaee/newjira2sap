$(document).on("change", "#project-single-select", function() {
	resetField("SAP-Aufgabe", "tasks");
	resetField("SAP-Serviceart", "products");
	resetField("SAP-Serviceart-Ext", "products-ext");
	resetField('SAP-Bestellpos', 'purchase-order');
});

$("a:contains('SAP')").live("click", function() {
	var baseUrl = baseURL();
	var theProjectKey = "";

	if ($("#project-options").length > 0) {
		theProjectKey = getProjectKey(baseUrl);
		mainFunction(baseUrl, theProjectKey);
	} else {
		$.ajax({
			url: baseUrl + "/rest/api/2/issue/" + window.jira.app.issue.getIssueKey(),
			method: "GET",
		}).done(function(data) {
			theProjectKey = data["fields"]["project"]["key"];
			mainFunction(baseUrl, theProjectKey);
		})
	}
});

function mainFunction(baseUrl, theProjectKey) {
	var sapAufgabeId = $("label:contains('SAP-Aufgabe')").attr("for");
	var sapAufgabeElement = $("#" + sapAufgabeId);

	if (!sapAufgabeElement.attr("list")) {
		sapAufgabeElement.attr("list", "tasks");
		sapAufgabeElement.addClass("jira2sap-dropdown-arrow");
		sapAufgabeElement.after("<span class='jira2sap-extra-arrow'>");
		firefoxHandler(sapAufgabeElement);

		var pcId = new Array();
		var customerId = "";
		var projectId = "";

		getProjectCustomerID(baseUrl, theProjectKey).then(function(data) {
			$.each(data, function(index, key) {
				if (data[index]["propertyKey"] == "customerNo") {
					customerId = data[index]["propertyValue"];
				}
				if (data[index]["propertyKey"] == "SAPProject") {
					projectId = data[index]["propertyValue"];
				}
			});
			if (!customerId == "") pcId.push(customerId);
			if (!projectId == "") pcId.push(projectId);
			if (pcId.length > 1) {
				$.ajax({
					type: "GET",
					url: baseUrl + '/rest/scriptrunner/latest/custom/sapTasks?customerId=' + pcId[0] + "&projectId=" + pcId[1],
				}).done(function(newData) {
					sapAufgabeElement.after(dataListGenerator(newData['items'], "tasks"));
				});
			} else {
				theElement.val("Keine SAP Aufgaben");
				theElement.attr("disabled", "disabled");
			}
		});
	}

	var serviceArtId = $("label:contains('SAP-Serviceart')").attr("for");
	var serviceArtElement = $("#" + serviceArtId);
	if (!serviceArtElement.attr("list")) {
		serviceArtElement.attr("list", "products");
		serviceArtElement.addClass("jira2sap-dropdown-arrow");
		serviceArtElement.after("<span class='jira2sap-extra-arrow'>");
		firefoxHandler(serviceArtElement);

		serviceProduct(baseUrl).then(function(data) {
			serviceArtElement.html(dataListGenerator(data["items"], "products"));
		});
	}

	var serviceArtExtId = $("label:contains('SAP-Serviceart-Ext')").attr("for");
	var serviceArtExElement = $("#" + serviceArtExtId);
	if (!serviceArtExElement.attr("list")) {
		serviceArtExElement.attr("list", "products-ext");
		serviceArtExElement.addClass("jira2sap-dropdown-arrow");
		serviceArtExElement.after("<span class='jira2sap-extra-arrow'>");
		firefoxHandler(serviceArtExElement);

		serviceProduct(baseUrl).then(function(data) {
			serviceArtExElement.html(dataListGenerator(data["items"], "products-ext"));
		});
	}

	var sapBestellposId = $("label:contains('SAP-Bestellpos')").attr("for");
	var sapBestellposElement = $("#" + sapBestellposId);
	if (!sapBestellposElement.attr("list")) {
		sapBestellposElement.addClass("jira2sap-dropdown-arrow");
		sapBestellposElement.after("<span class='jira2sap-extra-arrow'>");
		firefoxHandler(sapBestellposElement);

		bestellposFunction(sapBestellposElement, "purchase-order", baseUrl, theProjectKey);
	}
}

function getProjectCustomerID(baseUrl, theProjectKey) {
	return Promise.resolve($.ajax({
		url: baseUrl + "/rest/projectproperties/1.0/property/list/" + theProjectKey,
		method: "GET",
	}));
}

function baseURL() {
	var getUrl = window.location;
	return getUrl.protocol + "//" + getUrl.host;
}

function dataListGenerator(data, theId) {
	var dataArray = new Array();
	$.each(data, function(index) {
		dataArray.push(data[index]["value"]);
	});
	dataArray.sort();
	var dataListString = "<datalist id='" + theId + "'>";
	$.each(dataArray, function(index) {
		dataListString = dataListString.concat('<option value="');
		dataListString = dataListString.concat(dataArray[index]);
		dataListString = dataListString.concat('"></option>');
	});
	dataListString = dataListString.concat("</datalist>");
	return dataListString;
}

function getProjectKey(baseUrl) {
	var myArray = new Array();
	myArray = JSON.parse($("#project-options").attr("data-suggestions"));
	var projectKey = myArray[0]["items"][0]["label"];
	projectKey = /\(.*\)/.exec(projectKey);
	projectKey = String(projectKey);
	projectKey = projectKey.replace(/[{()}]/g, '');
	return projectKey
}

function serviceProduct(baseUrl) {
	return Promise.resolve($.ajax({
		url: baseUrl + "/rest/scriptrunner/latest/custom/sapServiceTypes",
		method: "GET",
	}));
}

function getProjectId(baseUrl, theProjectKey) {
	return Promise.resolve($.ajax({
		url: baseUrl + "/rest/projectproperties/1.0/property/list/" + theProjectKey,
		method: "GET",
	}));
}

function bestellposFunction(theThis, dataListId, baseUrl, theProjectKey) {
	var thisElement = theThis;
	var projectId = "";
	getProjectId(baseUrl, theProjectKey).then(function(data) {
		$.each(data, function(index, key) {
			if (data[index]["propertyKey"] == "SAPProject") {
				projectId = data[index]["propertyValue"];
			}
		});
		if (projectId != "") {
			thisElement.attr("list", dataListId);
			$.ajax({
				type: "GET",
				url: baseUrl + '/rest/scriptrunner/latest/custom/sapPurchaseOrders?projectId=' + projectId + "&projectTask=&serviceType="
			}).done(function(newData) {
				if (newData["items"].length > 0) {
					thisElement.after(dataListGenerator(newData["items"], dataListId));
				} else {
					thisElement.val("Keine Bestellpositionen");
					thisElement.attr("disabled", "disabled");
				}
			});
		} else {
			thisElement.val("Keine SAP Aufgaben");
			thisElement.attr("disabled", "disabled");
		}
	});
}

function resetField(theName, elementID) {
	var theId = $("label:contains('" + theName + "')").attr("for");
	$("#" + theId).removeAttr("list");
	$("#" + theId).val("");
	$("#" + theId).removeAttr("disabled");
	$("#" + elementID).remove();
}

function firefoxHandler(theElement) {
	if (navigator.userAgent.search("Firefox") >= 0) {
		theElement.on("mouseover", function() {
			theElement.focus();
		});
	}
}